import React, { Component } from 'react';
import { Formik, Form, Field, ErrorMessage, } from 'formik';
import { Link } from 'react-router-dom';

import { connect } from "react-redux";
import {salvarPessoa, buscarPessoa} from "../../redux/actions/PessoaActions";
import {pessoaItemSelector} from "../../redux/selectors/PessoaSelectors";

var initialValues = {
    nome: ""
}

class PessoaForm extends React.Component{

    constructor(props){
        super(props);

        const { id } = this.props.match.params;

        if(id){
            this.props.buscarPessoa(id);
        }

    }

    handleSubmit = (values, {setSubmitting}, props) => {

        this.props.salvarPessoa(values);
        setSubmitting(false);
        this.props.history.push('/pessoa/list');
    }

    render() {

        return(

            <div>
                
                <h1>Formulário de pessoa</h1>

                <Formik
                    initialValues= { this.props.match.params.id ? this.props.pessoaItem : initialValues }
                    enableReinitialize
                    onSubmit={ (values, actions) =>  this.handleSubmit(values, actions, this.props)  }
                >

                    { ({values, handleSubmit, isSubmitting}) => (

                        <Form onSubmit={handleSubmit}>

                            <div>
                                <label>Nome:</label>
                                <Field type="text" name="nome" />
                                <ErrorMessage name="marca" component="div"/>
                            </div>

                            <div>
                                <button type="submit" className="btn btn-primary" disabled={isSubmitting}>Salvar</button>
                                <Link to="/pessoa/list" className="btn btn-danger">Cancelar</Link>
                            </div>

                        </Form>
                    )}

                    
                </Formik>

            </div>

        );

    }

}

export default connect(pessoaItemSelector,{salvarPessoa, buscarPessoa})(PessoaForm);