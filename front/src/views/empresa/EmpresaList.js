import React, { Component } from 'react';
import { connect } from "react-redux";

import InputGroup from 'react-bootstrap/InputGroup';
import Form from 'react-bootstrap/Form';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

import { Link } from 'react-router-dom';

import {listarEmpresa, excluirEmpresa} from "../../redux/actions/EmpresaActions";
import {empresaListaSelector} from "../../redux/selectors/EmpresaSelectors";

class EmpresaList extends React.Component{

    constructor(props, context){
        super(props, context);

        this.props.listarEmpresa();

        this.state = {
            search: "",
            modalShow: false,
            empresa: {}
        }

        console.log("Empres Lista = ", this.props.empresaLista)
    }

    handleExcluir = () => {
        this.props.excluirEmpresa(this.state.empresa.id)
        .then( () => {
            this.handleClose();
        })
        .catch(); 
    }

    handleClose = () =>{
        this.setState({modalShow: false});
    }

    handleShow = (empresaItem) => {
        this.setState({modalShow: true});
        this.setState({empresa: empresaItem});
    }

    handleSearch = (event) => {
        this.setState({search: event.target.value});
    }

    render(){
        return(
            <div>
                <h1> Lista de empresas</h1>

                <InputGroup className="mb-3">
                <Form.Control type="search" onChange={this.handleSearch} placeholder="Pesquisar pelo nome" />
                <InputGroup.Append>
                    <InputGroup.Text id="basic-addon1">Filtar</InputGroup.Text>
                </InputGroup.Append>
                </InputGroup>

                <table className="table">
                    <thead>
                        <tr>
                            <th scope="col">#Id</th>
                            <th scope="col">Nome</th>
                            <th scope="col">CNPJ</th>
                            <th scope="col">Operações</th>
                        </tr>
                    </thead>
                    <tbody>

                        { this.props.empresaLista
                        .filter( empresa => empresa.nome.toLowerCase().includes(this.state.search.toLowerCase()) )
                        .map( (empresa,index) => 
                            
                            <tr key={index}>
                                <td>{empresa.id}</td>
                                <td>{empresa.nome}</td>
                                <td>{empresa.cnpj}</td>
                                <td>
                                <Link to={`/empresa/form/${empresa.id}`} className="btn btn-primary">Alterar</Link>
                                     | 
                                <Button variant="primary" onClick={() => this.handleShow(empresa)}>
                                    Excluir
                                </Button>
                                </td>
                            </tr>    
                              
                        )}


                    </tbody>
                </table>

                <Link to="/empresa/form" className="btn btn-primary">Novo</Link>

                <Modal show={this.state.modalShow} onHide={this.handleClose} animation={false}>
                    <Modal.Header closeButton>
                        <Modal.Title>Deseja excluir a empresa {this.state.empresa.nome}</Modal.Title>
                    </Modal.Header>
                    <Modal.Footer>
                    <Button variant="secondary" onClick={this.handleClose}>
                        Cancelar
                    </Button>
                    <Button variant="danger" onClick={this.handleExcluir}>
                        Excluir
                    </Button>
                    </Modal.Footer>
                </Modal>

            </div>
        );
    }

}

export default connect(empresaListaSelector, {listarEmpresa, excluirEmpresa})(EmpresaList);