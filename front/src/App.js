import React from 'react';
import logo from './logo.svg';
import './App.css';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import {Provider} from "react-redux";
import store from "./redux/store"


import PessoaLista from "./views/pessoa/PessoaList";
import PessoaForm from "./views/pessoa/PessoaForm";
import EmpresaForm from "./views/empresa/EmpresaForm";
import EmpresaList from "./views/empresa/EmpresaList";


function App() {
  return (

    <Provider store={store}>
      <Router>

        {/* TOPO  */}
        <Navbar bg="dark" variant="dark">
          <Navbar.Brand href="#home">Navbar</Navbar.Brand>
          <Nav className="mr-auto">
            <Nav.Link href="#home">Home</Nav.Link>
            <Nav.Link href="#features">Features</Nav.Link>
            <Nav.Link href="#pricing">Pricing</Nav.Link>
          </Nav>
        </Navbar>

        {/* CONTEUDO  */}
        <div>

          <Container fluid>
            <Row>

              {/* MENU VERTICAL  */}
              <Col lg={2}>
                <Nav defaultActiveKey="/home" className="flex-column">
                  <Link to="/carros">Carros</Link>
                  <Link to="/motos">Motos</Link>
                  <Link to="/empresa/list">Empresa</Link>
                  <Link to="/pessoa/list">Pessoa</Link>
                </Nav>
              </Col>

              {/* CONTEUDO DINÂMICO  */}
              <Col lg={10}>
                <Switch>
                  <Route exact path="/empresa/list"  component={EmpresaList} />
                  <Route exact path="/empresa/form"  component={EmpresaForm} />
                  <Route exact path="/empresa/form/:id"  component={EmpresaForm} />

                  <Route exact path="/pessoa/list"  component={PessoaLista} />
                  <Route exact path="/pessoa/form"  component={PessoaForm} />
                  <Route exact path="/pessoa/form/:id"  component={PessoaForm} />
                </Switch>
              </Col>

            </Row>
          </Container>

        </div>

        {/* RODAPÉ  */}

        <Container fluid style={{backgroundColor: "#dddddd"}}>
          <Row>

            <Col md={4}>
              <Nav defaultActiveKey="/home" className="flex-column">
                <Nav.Link href="/home">Active</Nav.Link>
                <Nav.Link eventKey="link-1">Link</Nav.Link>
                <Nav.Link eventKey="link-2">Link</Nav.Link>
                <Nav.Link eventKey="disabled" disabled>
                  Disabled
                </Nav.Link>
              </Nav>
            </Col>

            <Col md={{ span: 4, offset: 4 }}>
              <h2>Contato</h2>
              <p>store@maverick.com</p>
              <p>(62) 3999-9996</p>
              <p>Rua sem saída, lt 8, qd. 0</p>
            </Col>

          </Row>
        </Container>


      </Router>
    </Provider>
  );
}

export default App;
